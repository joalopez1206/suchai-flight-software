/**
 * @file init.h
 * @author Carlos Gonzalez C - carlgonz@uchile.cl
 * @author Matias Ramirez M - nicoram.mt@gmail.com
 * @date 2020
 * @copyright GNU GPL v3
 *
 * Initialization functions
 */

#ifndef SCH_INIT_H
#define SCH_INIT_H

#include <stdio.h>
#include <signal.h>

/**
 * Performs initialization actions
 */
void on_reset(void);

#endif //SCH_INIT_H

